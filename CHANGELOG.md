# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.3.0] - 2020-05-26
### Added
- Add ICD 10 GM-2020
- Add OPS-2020
### Changed
- Add "operator" info as new slot for i2b2 icd10/ops
- Add i2b2 path info for icd categories

## [1.2.0] - 2019-01-18
### Changed
- change parent pom groupid to de.mig.samply
- update samply dependencies
- change samply dependency groupids to de.mig.samply