<?xml version="1.0" encoding="UTF-8" ?>
<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:mdr="http://mdr.samply.de" version="2.0"
    xmlns="http://schema.samply.de/mdr/common">

    <xsl:output method="xml" omit-xml-declaration="no" byte-order-mark="no" encoding="UTF-8" indent="yes" />

    <xsl:template match="/">
        <xsl:element name="catalog">
            <xsl:element name="definitions">
                <xsl:element name="definition">
                    <xsl:attribute name="lang">de</xsl:attribute>
                    <xsl:element name="designation">ISO 3166-1 Alpha 2 (erweitert)</xsl:element>
                    <xsl:element name="definition">ISO 3166-1 Alpha 2 ist ein Standard für die zweistellige Kodierung von Staaten, herausgegeben von der Internationalen Organisation für Normung (ISO). Diese Liste enthält weitere Länder, die in der ISO-Liste nicht vorkommen.</xsl:element>
                </xsl:element>

                <xsl:element name="definition">
                    <xsl:attribute name="lang">en</xsl:attribute>
                    <xsl:element name="designation">ISO 3166-1 Alpha 2 (extended)</xsl:element>
                    <xsl:element name="definition">ISO 3166-1 Alpha 2 is a standard for two letter country codes, released by the International Organization for Standardization. This list includes additional countries not part of the ISO list.</xsl:element>
                </xsl:element>

                <xsl:element name="definition">
                    <xsl:attribute name="lang">es</xsl:attribute>
                    <xsl:element name="designation">ISO 3166-1 Alpha 2 (extendido)</xsl:element>
                    <xsl:element name="definition">ISO 3166-1 Alpha 2 es un estándar para la codificación de dos dígitos de los estados emitida por la Organización Internacional de Normalización (ISO). Esta lista contiene otros países que no aparecen en la lista ISO.</xsl:element>
                </xsl:element>

                <xsl:element name="definition">
                    <xsl:attribute name="lang">fr</xsl:attribute>
                    <xsl:element name="designation">ISO 3166-1 Alpha 2 (étendu)</xsl:element>
                    <xsl:element name="definition">L'ISO 3166-1 Alpha 2 est une norme de codage à deux chiffres des États publiée par l'Organisation internationale de normalisation (ISO). Cette liste contient d'autres pays qui ne figurent pas dans la liste ISO.</xsl:element>
                </xsl:element>
            </xsl:element>

            <xsl:call-template name="continent">
                <xsl:with-param name="name-de">Europa</xsl:with-param>
                <xsl:with-param name="name-en">Europe</xsl:with-param>
                <xsl:with-param name="name-es">Europa</xsl:with-param>
                <xsl:with-param name="name-fr">Europe</xsl:with-param>
            </xsl:call-template>

            <xsl:call-template name="continent">
                <xsl:with-param name="name-de">Afrika</xsl:with-param>
                <xsl:with-param name="name-en">Africa</xsl:with-param>
                <xsl:with-param name="name-es">África</xsl:with-param>
                <xsl:with-param name="name-fr">Afrique</xsl:with-param>
            </xsl:call-template>

            <xsl:call-template name="continent">
                <xsl:with-param name="name-de">Asien</xsl:with-param>
                <xsl:with-param name="name-en">Asia</xsl:with-param>
                <xsl:with-param name="name-es">Asia</xsl:with-param>
                <xsl:with-param name="name-fr">Asie</xsl:with-param>
            </xsl:call-template>

            <xsl:call-template name="continent">
                <xsl:with-param name="name-de">Amerika</xsl:with-param>
                <xsl:with-param name="name-en">Americas</xsl:with-param>
                <xsl:with-param name="name-es">América</xsl:with-param>
                <xsl:with-param name="name-fr">Amérique</xsl:with-param>
            </xsl:call-template>

            <xsl:call-template name="continent">
                <xsl:with-param name="name-de">Ozeanien</xsl:with-param>
                <xsl:with-param name="name-en">Oceania</xsl:with-param>
                <xsl:with-param name="name-es">Oceania</xsl:with-param>
                <xsl:with-param name="name-fr">Océanie</xsl:with-param>
            </xsl:call-template>

            <xsl:call-template name="continent">
                <xsl:with-param name="name-de">Andere</xsl:with-param>
                <xsl:with-param name="name-en">Other areas</xsl:with-param>
                <xsl:with-param name="name-es">Otras áreas</xsl:with-param>
                <xsl:with-param name="name-fr">Autres endroits</xsl:with-param>
            </xsl:call-template>

            <xsl:for-each select="//country">
                <xsl:element name="code">
                    <xsl:attribute name="code">
                        <xsl:value-of select="cca2"/>
                    </xsl:attribute>
                    <xsl:attribute name="isValid">true</xsl:attribute>

                    <xsl:element name="definitions">
                        <xsl:element name="definition">
                            <xsl:attribute name="lang">en</xsl:attribute>
                            <xsl:element name="designation">
                                <xsl:value-of select="name/common" />
                            </xsl:element>
                            <xsl:element name="definition">
                                <xsl:value-of select="concat('(', cca2/text(), ')')" />
                                <xsl:value-of select="' '" />
                                <xsl:value-of select="name/official" />
                            </xsl:element>
                        </xsl:element>
                        <xsl:element name="definition">
                            <xsl:attribute name="lang">de</xsl:attribute>
                            <xsl:element name="designation">
                                <xsl:value-of select="translations/deu/common" />
                            </xsl:element>
                            <xsl:element name="definition">
                                <xsl:value-of select="concat('(', cca2/text(), ')')" />
                                <xsl:value-of select="' '" />
                                <xsl:value-of select="translations/deu/official" />
                            </xsl:element>
                        </xsl:element>
                        <xsl:element name="definition">
                            <xsl:attribute name="lang">es</xsl:attribute>
                            <xsl:element name="designation">
                                <xsl:value-of select="translations/spa/common" />
                            </xsl:element>
                            <xsl:element name="definition">
                                <xsl:value-of select="concat('(', cca2/text(), ')')" />
                                <xsl:value-of select="' '" />
                                <xsl:value-of select="translations/spa/official" />
                            </xsl:element>
                        </xsl:element>
                        <xsl:element name="definition">
                            <xsl:attribute name="lang">fr</xsl:attribute>
                            <xsl:element name="designation">
                                <xsl:value-of select="translations/fra/common" />
                            </xsl:element>
                            <xsl:element name="definition">
                                <xsl:value-of select="concat('(', cca2/text(), ')')" />
                                <xsl:value-of select="' '" />
                                <xsl:value-of select="translations/fra/official" />
                            </xsl:element>
                        </xsl:element>
                    </xsl:element>
                </xsl:element>
            </xsl:for-each>
        </xsl:element>
    </xsl:template>

    <xsl:template name="continent">
        <xsl:param name="name-de"/>
        <xsl:param name="name-en"/>
        <xsl:param name="name-es"/>
        <xsl:param name="name-fr"/>

        <xsl:element name="code">
            <xsl:attribute name="code">
                <xsl:value-of select="$name-en"/>
            </xsl:attribute>
            <xsl:attribute name="isValid">false</xsl:attribute>
            <xsl:element name="definitions">
                <xsl:element name="definition">
                    <xsl:attribute name="lang">de</xsl:attribute>
                    <xsl:element name="designation">
                        <xsl:value-of select="$name-de" />
                    </xsl:element>
                    <xsl:element name="definition">
                        <xsl:value-of select="'Die Länder in '"/>
                        <xsl:value-of select="$name-de" />
                    </xsl:element>
                </xsl:element>
                <xsl:element name="definition">
                    <xsl:attribute name="lang">en</xsl:attribute>
                    <xsl:element name="designation">
                        <xsl:value-of select="$name-en" />
                    </xsl:element>
                    <xsl:element name="definition">
                        <xsl:value-of select="'The countries in '"/>
                        <xsl:value-of select="$name-en" />
                    </xsl:element>
                </xsl:element>
                <xsl:element name="definition">
                    <xsl:attribute name="lang">es</xsl:attribute>
                    <xsl:element name="designation">
                        <xsl:value-of select="$name-es" />
                    </xsl:element>
                    <xsl:element name="definition">
                        <xsl:value-of select="'Los paises de '"/>
                        <xsl:value-of select="$name-es" />
                    </xsl:element>
                </xsl:element>
                <xsl:element name="definition">
                    <xsl:attribute name="lang">fr</xsl:attribute>
                    <xsl:element name="designation">
                        <xsl:value-of select="'l'''" />
                        <xsl:value-of select="$name-fr" />
                    </xsl:element>
                    <xsl:element name="definition">
                        <xsl:value-of select="'Les pays d'''"/>
                        <xsl:value-of select="$name-fr" />
                    </xsl:element>
                </xsl:element>
            </xsl:element>

            <xsl:if test="$name-en != 'Other areas'">
                <xsl:for-each select="//country/region[text()=$name-en]">
                    <xsl:element name="subCode">
                        <xsl:attribute name="code" select="../cca2"/>
                    </xsl:element>
                </xsl:for-each>
            </xsl:if>
            <xsl:if test="$name-en = 'Other areas'">
                <xsl:for-each select="//country/region[not(text()='Asia') and not(text()='Americas')
                    and not(text()='Africa') and not(text()='Europe') and not(text()='Oceania')]">
                    <xsl:element name="subCode">
                        <xsl:attribute name="code" select="../cca2"/>
                    </xsl:element>
                </xsl:for-each>
            </xsl:if>
        </xsl:element>
    </xsl:template>

    <xsl:template name="chooseContent">
        <xsl:choose>
            <xsl:when test="contains(.,'/direction')">
                <xsl:value-of select="substring-before(.,'/direction')"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="."/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
</xsl:transform>

