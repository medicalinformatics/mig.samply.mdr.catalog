<?xml version="1.0" encoding="UTF-8" ?>
<!--

    Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
    Contact: info@osse-register.de

    This program is free software; you can redistribute it and/or modify it under
    the terms of the GNU Affero General Public License as published by the Free
    Software Foundation; either version 3 of the License, or (at your option) any
    later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
    details.

    You should have received a copy of the GNU Affero General Public License
    along with this program; if not, see <http://www.gnu.org/licenses>.

    Additional permission under GNU GPL version 3 section 7:

    If you modify this Program, or any covered work, by linking or combining it
    with Jersey (https://jersey.java.net) (or a modified version of that
    library), containing parts covered by the terms of the General Public
    License, version 2.0, the licensors of this Program grant you additional
    permission to convey the resulting work.

-->
<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:mdr="http://mdr.samply.de" version="2.0"
    xmlns="http://schema.samply.de/mdr/common">

    <xsl:output method="xml" omit-xml-declaration="no" byte-order-mark="no" encoding="UTF-8" indent="yes" />

    <xsl:template match="/">
        <xsl:element name="catalog">
            <xsl:element name="definitions">
                <xsl:element name="definition">
                    <xsl:attribute name="lang">de</xsl:attribute>
                    <xsl:element name="designation">ISO 3166-1 Alpha 2 (erweitert)</xsl:element>
                    <xsl:element name="definition">ISO 3166-1 Alpha 2 ist ein Standard für die zweistellige Kodierung von Staaten, herausgegeben von der Internationalen Organisation für Normung (ISO). Diese Liste enthält weitere Länder, die in der ISO-Liste nicht vorkommen.</xsl:element>
                </xsl:element>

                <xsl:element name="definition">
                    <xsl:attribute name="lang">en</xsl:attribute>
                    <xsl:element name="designation">ISO 3166-1 Alpha 2 (extended)</xsl:element>
                    <xsl:element name="definition">ISO 3166-1 Alpha 2 is a standard for two letter country codes, released by the International Organization for Standardization. This list includes additional countries not part of the ISO list.</xsl:element>
                </xsl:element>
            </xsl:element>

            <xsl:call-template name="continent">
                <xsl:with-param name="name-de">Europa</xsl:with-param>
                <xsl:with-param name="name-en">Europe</xsl:with-param>
            </xsl:call-template>

            <xsl:call-template name="continent">
                <xsl:with-param name="name-de">Afrika</xsl:with-param>
                <xsl:with-param name="name-en">Africa</xsl:with-param>
            </xsl:call-template>

            <xsl:call-template name="continent">
                <xsl:with-param name="name-de">Asien</xsl:with-param>
                <xsl:with-param name="name-en">Asia</xsl:with-param>
            </xsl:call-template>

            <xsl:call-template name="continent">
                <xsl:with-param name="name-de">Amerika</xsl:with-param>
                <xsl:with-param name="name-en">Americas</xsl:with-param>
            </xsl:call-template>

            <xsl:call-template name="continent">
                <xsl:with-param name="name-de">Ozeanien</xsl:with-param>
                <xsl:with-param name="name-en">Oceania</xsl:with-param>
            </xsl:call-template>

            <xsl:call-template name="continent">
                <xsl:with-param name="name-de">Andere</xsl:with-param>
                <xsl:with-param name="name-en">Other areas</xsl:with-param>
            </xsl:call-template>

            <xsl:for-each select="//country">
                <xsl:element name="code">
                    <xsl:attribute name="code">
                        <xsl:value-of select="@cca2"/>
                    </xsl:attribute>
                    <xsl:attribute name="isValid">true</xsl:attribute>

                    <xsl:element name="definitions">
                        <xsl:element name="definition">
                            <xsl:attribute name="lang">en</xsl:attribute>
                            <xsl:element name="designation">
                                <xsl:value-of select="substring-before(@name, ',')" />
                            </xsl:element>
                            <xsl:element name="definition">
                                <xsl:value-of select="concat('(', @cca2, ')')" />
                                <xsl:value-of select="' '" />
                                <xsl:value-of select="substring-before(substring-after(@name, ','), ',')" />
                            </xsl:element>
                        </xsl:element>
                    </xsl:element>
                </xsl:element>
            </xsl:for-each>
        </xsl:element>
    </xsl:template>

    <xsl:template name="continent">
        <xsl:param name="name-de"/>
        <xsl:param name="name-en"/>

        <xsl:element name="code">
            <xsl:attribute name="code">
                <xsl:value-of select="$name-en"/>
            </xsl:attribute>
            <xsl:attribute name="isValid">false</xsl:attribute>
            <xsl:element name="definitions">
                <xsl:element name="definition">
                    <xsl:attribute name="lang">de</xsl:attribute>
                    <xsl:element name="designation">
                        <xsl:value-of select="$name-de" />
                    </xsl:element>
                    <xsl:element name="definition">
                        <xsl:value-of select="'Die Länder in '"/>
                        <xsl:value-of select="$name-de" />
                    </xsl:element>
                </xsl:element>
                <xsl:element name="definition">
                    <xsl:attribute name="lang">en</xsl:attribute>
                    <xsl:element name="designation">
                        <xsl:value-of select="$name-en" />
                    </xsl:element>
                    <xsl:element name="definition">
                        <xsl:value-of select="'The countries in '"/>
                        <xsl:value-of select="$name-en" />
                    </xsl:element>
                </xsl:element>
            </xsl:element>

            <xsl:if test="$name-en != 'Other areas'">
                <xsl:for-each select="//country[@region=$name-en]">
                    <xsl:element name="subCode">
                        <xsl:attribute name="code" select="@cca2"/>
                    </xsl:element>
                </xsl:for-each>
            </xsl:if>
            <xsl:if test="$name-en = 'Other areas'">
                <xsl:for-each select="//country[not(@region='Asia') and not(@region='Americas')
                    and not(@region='Africa') and not(@region='Europe') and not(@region='Oceania')]">
                    <xsl:element name="subCode">
                        <xsl:attribute name="code" select="@cca2"/>
                    </xsl:element>
                </xsl:for-each>
            </xsl:if>
        </xsl:element>
    </xsl:template>

    <xsl:template name="chooseContent">
        <xsl:choose>
            <xsl:when test="contains(.,'/direction')">
                <xsl:value-of select="substring-before(.,'/direction')"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="."/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
</xsl:transform>

