<?xml version="1.0" encoding="UTF-8" ?>
<!--

    Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
    Contact: info@osse-register.de

    This program is free software; you can redistribute it and/or modify it under
    the terms of the GNU Affero General Public License as published by the Free
    Software Foundation; either version 3 of the License, or (at your option) any
    later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
    details.

    You should have received a copy of the GNU Affero General Public License
    along with this program; if not, see <http://www.gnu.org/licenses>.

    Additional permission under GNU GPL version 3 section 7:

    If you modify this Program, or any covered work, by linking or combining it
    with Jersey (https://jersey.java.net) (or a modified version of that
    library), containing parts covered by the terms of the General Public
    License, version 2.0, the licensors of this Program grant you additional
    permission to convey the resulting work.

-->
<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:mdr="http://mdr.samply.de" version="2.0"
    xmlns="http://schema.samply.de/mdr/common">

    <xsl:output method="xml" omit-xml-declaration="no" byte-order-mark="no" encoding="UTF-8" indent="yes" />

    <xsl:template match="/">
        <xsl:element name="catalog">
            <xsl:element name="definitions">
                <xsl:element name="definition">
                    <xsl:attribute name="lang">en</xsl:attribute>
                    <xsl:element name="designation">The two letter code for states in the US and Canada.</xsl:element>
                    <xsl:element name="definition">The two letter code for states in the US and Canada.</xsl:element>
                </xsl:element>
            </xsl:element>

            <xsl:for-each select="//state">
                <xsl:element name="code">
                    <xsl:attribute name="code">
                        <xsl:value-of select="@abbreviation"/>
                    </xsl:attribute>
                    <xsl:attribute name="isValid">true</xsl:attribute>

                    <xsl:element name="definitions">
                        <xsl:element name="definition">
                            <xsl:attribute name="lang">en</xsl:attribute>
                            <xsl:element name="designation">
                                <xsl:value-of select="@name" />
                            </xsl:element>
                            <xsl:element name="definition">
                                <xsl:value-of select="@abbreviation" />
                                <xsl:value-of select="', '" />
                                <xsl:value-of select="@name" />
                            </xsl:element>
                        </xsl:element>
                    </xsl:element>
                </xsl:element>
            </xsl:for-each>
        </xsl:element>
    </xsl:template>
</xsl:transform>

