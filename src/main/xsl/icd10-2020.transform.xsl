<?xml version="1.0" encoding="UTF-8" ?>
<!--

    Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
    Contact: info@osse-register.de

    This program is free software; you can redistribute it and/or modify it under
    the terms of the GNU Affero General Public License as published by the Free
    Software Foundation; either version 3 of the License, or (at your option) any
    later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
    details.

    You should have received a copy of the GNU Affero General Public License
    along with this program; if not, see <http://www.gnu.org/licenses>.

    Additional permission under GNU GPL version 3 section 7:

    If you modify this Program, or any covered work, by linking or combining it
    with Jersey (https://jersey.java.net) (or a modified version of that
    library), containing parts covered by the terms of the General Public
    License, version 2.0, the licensors of this Program grant you additional
    permission to convey the resulting work.

-->
<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:mdr="http://mdr.samply.de" version="2.0"
    xmlns="http://schema.samply.de/mdr/common">

    <xsl:output method="xml" omit-xml-declaration="no" byte-order-mark="no" encoding="UTF-8" indent="yes" />

    <xsl:template match="/">
        <xsl:element name="catalog">
            <xsl:element name="definitions">
                <xsl:element name="definition">
                    <xsl:attribute name="lang">de</xsl:attribute>
                    <xsl:element name="designation">ICD-10-GM 2020</xsl:element>
                    <xsl:element name="definition">
                        <xsl:value-of select="/ClaML/Meta[@name='titleLong']/@value"/>
                    </xsl:element>
                </xsl:element>
            </xsl:element>

            <xsl:element name="slots">
                <xsl:element name="slot">
                    <xsl:element name="key">COPYRIGHT</xsl:element>
                    <xsl:element name="value">
                        <xsl:value-of select="/ClaML/Meta[@name='copyright']/@value"/>
                    </xsl:element>
                </xsl:element>
            </xsl:element>

            <xsl:for-each select="//Class[@kind='category']">
                <xsl:variable name="element" select="."/>
                <xsl:variable name="crossAster" select="mdr:getUsageSign($element)"/>

                <xsl:if test="count(ModifiedBy)=1">
                    <xsl:variable name="modifierId" select="ModifiedBy/@code"/>

                    <xsl:for-each select="//ModifierClass[@modifier=$modifierId]">
                        <xsl:variable name="modifierUsage" select="mdr:getUsageSign(.)"/>
                        <xsl:variable name="code" select="@code"/>

                        <xsl:if test="count($element/ModifiedBy/ValidModifierClass)=0 or $element/ModifiedBy/ValidModifierClass[@code=$code]">
                            <xsl:call-template name="element">
                                <xsl:with-param name="element" select="$element"/>
                                <xsl:with-param name="modifier" select="@code"/>
                                <xsl:with-param name="modifierDesc" select="Rubric[@kind='preferred']/Label/text()"/>
                                <xsl:with-param name="crossAster">
                                    <xsl:value-of select="mdr:getUsageSign($element)"/>
                                    <xsl:if test="mdr:getUsageSign($element)=''">
                                        <xsl:value-of select="$modifierUsage"/>
                                    </xsl:if>
                                </xsl:with-param>
                                <xsl:with-param name="ignoreSub"><xsl:value-of select="true()"/></xsl:with-param>
                            </xsl:call-template>

                            <xsl:if test="not($element/@usage) and $modifierUsage=''">
                                <xsl:call-template name="element">
                                    <xsl:with-param name="element" select="$element"/>
                                    <xsl:with-param name="modifier" select="@code"/>
                                    <xsl:with-param name="modifierDesc" select="Rubric[@kind='preferred']/Label/text()"/>
                                    <xsl:with-param name="crossAster" select="'+'"/>
                                    <xsl:with-param name="ignoreSub"><xsl:value-of select="true()"/></xsl:with-param>
                                </xsl:call-template>
                            </xsl:if>
                        </xsl:if>
                    </xsl:for-each>
                </xsl:if>

                <xsl:if test="count(ModifiedBy)=2">
                    <xsl:variable name="modifierIdFirst" select="ModifiedBy[1]/@code"/>
                    <xsl:variable name="modifierIdSecond" select="ModifiedBy[2]/@code"/>

                    <xsl:for-each select="//ModifierClass[@modifier=$modifierIdFirst]">
                        <xsl:variable name="modifierFirst" select="@code"/>
                        <xsl:variable name="modifierFirstDesc" select="Rubric[@kind='preferred']/Label/text()"/>
                        <xsl:variable name="modifierFirstUsage" select="mdr:getUsageSign(.)"/>

                        <xsl:call-template name="element">
                            <xsl:with-param name="element" select="$element"/>
                            <xsl:with-param name="modifier" select="$modifierFirst"/>
                            <xsl:with-param name="modifierDesc" select="$modifierFirstDesc"/>
                            <xsl:with-param name="crossAster">
                                <xsl:value-of select="mdr:getUsageSign($element)"/>
                                <xsl:if test="mdr:getUsageSign($element)=''">
                                    <xsl:value-of select="$modifierFirstUsage"/>
                                </xsl:if>
                            </xsl:with-param>
                            <xsl:with-param name="ignoreSub"><xsl:value-of select="true()"/></xsl:with-param>
                        </xsl:call-template>

                        <xsl:if test="not($element/@usage) and $modifierFirstUsage=''">
                            <xsl:call-template name="element">
                                <xsl:with-param name="element" select="$element"/>
                                <xsl:with-param name="modifier" select="$modifierFirst"/>
                                <xsl:with-param name="modifierDesc" select="$modifierFirstDesc"/>
                                <xsl:with-param name="crossAster" select="'+'"/>
                                <xsl:with-param name="ignoreSub"><xsl:value-of select="true()"/></xsl:with-param>
                            </xsl:call-template>
                        </xsl:if>

                        <xsl:for-each select="//ModifierClass[@modifier=$modifierIdSecond]">
                            <xsl:variable name="modifierSecond" select="@code"/>
                            <xsl:variable name="modifierSecondDesc" select="Rubric[@kind='preferred']/Label/text()"/>
                            <xsl:variable name="modifierSecondUsage" select="mdr:getUsageSign(.)"/>

                            <xsl:if test="not(Meta[@name='excludeOnPrecedingModifier' and ends-with(@value, $modifierFirst)])">
                                <xsl:call-template name="element">
                                    <xsl:with-param name="element" select="$element"/>
                                    <xsl:with-param name="modifier">
                                        <xsl:value-of select="$modifierFirst"/>
                                        <xsl:value-of select="$modifierSecond"/>
                                    </xsl:with-param>
                                    <xsl:with-param name="modifierDesc">
                                        <xsl:value-of select="$modifierFirstDesc"/>
                                        <xsl:value-of select="', '"/>
                                        <xsl:value-of select="$modifierSecondDesc"/>
                                    </xsl:with-param>
                                    <xsl:with-param name="crossAster">
                                        <xsl:value-of select="mdr:getUsageSign($element)"/>
                                        <xsl:if test="mdr:getUsageSign($element)=''">
                                            <xsl:value-of select="$modifierFirstUsage"/>
                                            <xsl:value-of select="$modifierSecondUsage"/>
                                        </xsl:if>
                                    </xsl:with-param>
                                    <xsl:with-param name="ignoreSub"><xsl:value-of select="true()"/></xsl:with-param>
                                </xsl:call-template>

                                <xsl:if test="not($element/@usage) and $modifierFirstUsage='' and $modifierSecondUsage=''">
                                    <xsl:call-template name="element">
                                        <xsl:with-param name="element" select="$element"/>
                                        <xsl:with-param name="modifier">
                                            <xsl:value-of select="$modifierFirst"/>
                                            <xsl:value-of select="$modifierSecond"/>
                                        </xsl:with-param>
                                        <xsl:with-param name="modifierDesc">
                                            <xsl:value-of select="$modifierFirstDesc"/>
                                            <xsl:value-of select="', '"/>
                                            <xsl:value-of select="$modifierSecondDesc"/>
                                        </xsl:with-param>
                                        <xsl:with-param name="crossAster" select="'+'"/>
                                        <xsl:with-param name="ignoreSub"><xsl:value-of select="true()"/></xsl:with-param>
                                    </xsl:call-template>
                                </xsl:if>
                            </xsl:if>
                        </xsl:for-each>
                    </xsl:for-each>
                </xsl:if>
            </xsl:for-each>

            <xsl:for-each select="//Class[@kind='category' or @kind='chapter' or @kind='block']">
                <xsl:variable name="element" select="."/>
                <xsl:call-template name="element">
                    <xsl:with-param name="element" select="."/>
                    <xsl:with-param name="modifier" select="''"/>
                    <xsl:with-param name="modifierDesc" select="''"/>
                    <xsl:with-param name="crossAster" select="mdr:getUsageSign($element)"/>
                    <xsl:with-param name="ignoreSub"><xsl:value-of select="false()"/></xsl:with-param>
                </xsl:call-template>

                <xsl:if test="mdr:isValid(.)=true() and not($element/@usage)">
                    <xsl:call-template name="element">
                        <xsl:with-param name="element" select="."/>
                        <xsl:with-param name="modifier" select="''"/>
                        <xsl:with-param name="modifierDesc" select="''"/>
                        <xsl:with-param name="crossAster" select="'+'"/>
                        <xsl:with-param name="ignoreSub"><xsl:value-of select="true()"/></xsl:with-param>
                    </xsl:call-template>
                </xsl:if>
            </xsl:for-each>
        </xsl:element>
    </xsl:template>

    <xsl:template name="element">
        <xsl:param name="modifier" />
        <xsl:param name="modifierDesc" />
        <xsl:param name="crossAster" />
        <xsl:param name="element" />
        <xsl:param name="ignoreSub" />
        <xsl:variable name="id" select="$element/@code"/>
        <xsl:variable name="code">
            <xsl:value-of select="$element/@code"/>
            <xsl:if test="$modifier != ''">
                <xsl:value-of select="$modifier"/>
            </xsl:if>
            <xsl:value-of select="$crossAster"/>
        </xsl:variable>
        <xsl:variable name="isValid" select="mdr:isValid($element)"/>

        <xsl:element name="code">
            <xsl:attribute name="code">
                <xsl:value-of select="$code"/>
            </xsl:attribute>

            <xsl:attribute name="isValid">
                <xsl:value-of select="$isValid"/>
            </xsl:attribute>

            <xsl:if test="$element/@kind='chapter'">
                <xsl:attribute name="order">
                    <xsl:value-of select="mdr:RomanToInteger($element/@code, 1)"/>
                </xsl:attribute>
            </xsl:if>

            <xsl:element name="definitions">
                <xsl:call-template name="definition-template">
                    <xsl:with-param name="element" select="$element"/>
                    <xsl:with-param name="code" select="$code" />
                    <xsl:with-param name="modifierDesc" select="$modifierDesc" />
                </xsl:call-template>
            </xsl:element>

            <xsl:if test="$ignoreSub=false()">
                <xsl:if test="$isValid=true() and $crossAster=''">
                    <xsl:call-template name="subclass-template">
                        <xsl:with-param name="subCode">
                            <xsl:value-of select="$code"/>
                            <xsl:value-of select="'+'"/>
                        </xsl:with-param>
                    </xsl:call-template>
                </xsl:if>
                <xsl:for-each select="SubClass">
                    <xsl:variable name="code" select="@code"/>
                    <xsl:call-template name="subclass-template">
                        <xsl:with-param name="subCode">
                            <xsl:value-of select="@code"/>
                            <xsl:value-of select="mdr:getUsageSign(//Class[@code=$code])"/>
                        </xsl:with-param>
                    </xsl:call-template>
                </xsl:for-each>

                <xsl:if test="$element/count(ModifiedBy)=1">
                    <xsl:variable name="modifierId" select="$element/ModifiedBy/@code"/>
                    <xsl:for-each select="//ModifierClass[@modifier=$modifierId]">
                        <xsl:variable name="modCode" select="@code"/>

                        <xsl:if test="count($element/ModifiedBy/ValidModifierClass)=0 or $element/ModifiedBy/ValidModifierClass[@code=$modCode]">

                            <xsl:call-template name="subclass-template">
                                <xsl:with-param name="subCode">
                                    <xsl:value-of select="$id"/>
                                    <xsl:value-of select="@code"/>
                                    <xsl:value-of select="mdr:getUsageSign($element)"/>
                                </xsl:with-param>
                            </xsl:call-template>

                            <xsl:if test="$crossAster=''">
                                <xsl:call-template name="subclass-template">
                                    <xsl:with-param name="subCode">
                                        <xsl:value-of select="$id"/>
                                        <xsl:value-of select="@code"/>
                                        <xsl:value-of select="'+'"/>
                                    </xsl:with-param>
                                </xsl:call-template>
                            </xsl:if>

                        </xsl:if>

                    </xsl:for-each>
                </xsl:if>

                <xsl:if test="count(ModifiedBy)=2">
                    <xsl:variable name="modifierIdFirst" select="ModifiedBy[1]/@code"/>
                    <xsl:variable name="modifierIdSecond" select="ModifiedBy[2]/@code"/>

                    <xsl:for-each select="//ModifierClass[@modifier=$modifierIdFirst]">
                        <xsl:variable name="modifierFirst" select="@code"/>
                        <xsl:variable name="modifierFirstNode" select="."/>

                        <xsl:call-template name="subclass-template">
                            <xsl:with-param name="subCode">
                                <xsl:value-of select="$id"/>
                                <xsl:value-of select="@code"/>
                                <xsl:value-of select="mdr:getUsageSign($element)"/>
                                <xsl:value-of select="mdr:getUsageSign($modifierFirstNode)"/>
                            </xsl:with-param>
                        </xsl:call-template>

                        <xsl:if test="$crossAster='' and mdr:getUsageSign(.)=''">
                            <xsl:call-template name="subclass-template">
                                <xsl:with-param name="subCode">
                                    <xsl:value-of select="$id"/>
                                    <xsl:value-of select="@code"/>
                                    <xsl:value-of select="'+'"/>
                                </xsl:with-param>
                            </xsl:call-template>
                        </xsl:if>

                        <xsl:for-each select="//ModifierClass[@modifier=$modifierIdSecond]">
                            <xsl:variable name="modifierSecond" select="@code"/>
                            <xsl:variable name="modifierSecondNode" select="."/>

                            <xsl:if test="not(Meta[@name='excludeOnPrecedingModifier' and ends-with(@value, $modifierFirst)])">
                                <xsl:call-template name="subclass-template">
                                    <xsl:with-param name="subCode">
                                        <xsl:value-of select="$id"/>
                                        <xsl:value-of select="$modifierFirst"/>
                                        <xsl:value-of select="$modifierSecond"/>
                                        <xsl:value-of select="mdr:getUsageSign($element)"/>
                                        <xsl:value-of select="mdr:getUsageSign($modifierFirstNode)"/>
                                        <xsl:value-of select="mdr:getUsageSign($modifierSecondNode)"/>
                                    </xsl:with-param>
                                </xsl:call-template>

                                <xsl:if test="$crossAster='' and mdr:getUsageSign($modifierFirstNode)='' and mdr:getUsageSign($modifierSecondNode)=''">
                                    <xsl:call-template name="subclass-template">
                                        <xsl:with-param name="subCode">
                                            <xsl:value-of select="$id"/>
                                            <xsl:value-of select="$modifierFirst"/>
                                            <xsl:value-of select="$modifierSecond"/>
                                            <xsl:value-of select="'+'"/>
                                        </xsl:with-param>
                                    </xsl:call-template>
                                </xsl:if>
                            </xsl:if>
                        </xsl:for-each>
                    </xsl:for-each>
                </xsl:if>

            </xsl:if>
        </xsl:element>
    </xsl:template>

    <xsl:template name="subclass-template">
        <xsl:param name="subCode" />
        <xsl:element name="subCode">
            <xsl:attribute name="code">
                <xsl:value-of select="$subCode"/>
            </xsl:attribute>
        </xsl:element>
    </xsl:template>

    <xsl:template name="definition-template">
        <xsl:param name="code" />
        <xsl:param name="modifierDesc"/>
        <xsl:param name="element" />
        <xsl:element name="definition">
            <xsl:attribute name="lang">de</xsl:attribute>
            <xsl:element name="designation">
                <xsl:value-of select="$element/Rubric[@kind='preferred']/Label/text()"/>
            </xsl:element>
            <xsl:element name="definition">
                <xsl:value-of select="$code" />
                <xsl:value-of select="', '" />
                <xsl:choose>
                    <xsl:when test="$element/count(Rubric[@kind='preferredLong'])&gt;0">
                        <xsl:value-of select="$element/Rubric[@kind='preferredLong']/Label/text()"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="$element/Rubric[@kind='preferred']/Label/text()"/>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:if test="$modifierDesc != ''">
                    <xsl:value-of select="', '" />
                    <xsl:value-of select="$modifierDesc"/>
                </xsl:if>
            </xsl:element>
        </xsl:element>
    </xsl:template>

    <xsl:function name="mdr:getUsageSign">
        <xsl:param name="element"/>
        <xsl:value-of>
            <xsl:choose>
                <xsl:when test="$element/@usage='dagger'">+</xsl:when>
                <xsl:when test="$element/@usage='optional'">!</xsl:when>
                <xsl:when test="$element/@usage='aster'">*</xsl:when>
                <xsl:otherwise></xsl:otherwise>
            </xsl:choose>
        </xsl:value-of>
    </xsl:function>

    <xsl:function name="mdr:isValid">
        <xsl:param name="element"/>
        <xsl:value-of>
            <xsl:choose>
                <xsl:when test="$element[@kind='category']">
                    <xsl:value-of select="true()"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="false()"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:value-of>
    </xsl:function>

    <xsl:function name="mdr:RomanToInteger">
        <xsl:param name="roman_number" />
        <xsl:param name="index" />
        <xsl:variable name="temp">
            <xsl:value-of select="mdr:toRoman($index)"/>
        </xsl:variable>
        <xsl:choose>
            <xsl:when test="$temp = $roman_number">
                <xsl:value-of select="$index" />
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="mdr:RomanToInteger($roman_number, $index + 1)"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>

    <xsl:function name="mdr:toRoman">
        <xsl:param name="value" />
        <xsl:number value="$value" format="I" />
    </xsl:function>
</xsl:transform>

