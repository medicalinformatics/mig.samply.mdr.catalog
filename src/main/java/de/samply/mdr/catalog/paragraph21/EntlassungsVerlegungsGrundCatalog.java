/*
 * Copyright (C) 2019 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.catalog.paragraph21;

import de.samply.config.util.JaxbUtil;
import de.samply.mdr.xsd.Catalog;
import de.samply.mdr.xsd.Code;
import de.samply.mdr.xsd.Code.SubCode;
import de.samply.mdr.xsd.Definition;
import de.samply.mdr.xsd.Definitions;
import de.samply.mdr.xsd.ObjectFactory;
import de.samply.mdr.xsd.Slot;
import de.samply.mdr.xsd.Slots;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

public class EntlassungsVerlegungsGrundCatalog {

  public static final String RESULT_FILENAME =
      "target/generated-resources/xml/EntlassungsVerlegungsGrund.xml";
  private static final String SLOTNAME_I2B2_OPERATOR = "I2B2_OPERATOR";
  private static final String SLOTNAME_I2B2_PATH = "I2B2_PATH";
  private static final String SLOTNAME_I2B2_ICON = "I2B2_ICON";
  private static final String SLOTNAME_I2B2_HLEVEL = "I2B2_HLEVEL";

  private static final String SLOT_I2B2_PATH_PREFIX = "\\i2b2\\ADM\\ENTLGR\\";

  private static Map<Integer, String> digit1and2 = new HashMap<>();
  private static Map<Integer, String> digit3 = new HashMap<>();
  private static Set<Integer> codesWithAllSubcodes = new HashSet<>();

  static {
    digit1and2.put(1, "Behandlung regulär beendet");
    digit1and2.put(2, "Behandlung regulär beendet, nachstationäre Behandlung vorgesehen");
    digit1and2.put(3, "Behandlung aus sonstigen Gründen beendet");
    digit1and2.put(4, "Behandlung gegen ärztlichen Rat beendet");
    digit1and2.put(5, "Zuständigkeitswechsel des Kostenträgers");
    digit1and2.put(6, "Verlegung in ein anderes Krankenhaus");
    digit1and2.put(7, "Tod");
    digit1and2.put(8, "Verlegung in ein anderes Krankenhaus im Rahmen einer Zusammenarbeit");
    digit1and2.put(9, "Entlassung in eine Rehabilitationseinrichtung");
    digit1and2.put(10, "Entlassung in eine Pflegeeinrichtung");
    digit1and2.put(11, "Entlassung in ein Hospiz");
    //    digit1and2.put(12, "interne Verlegung");
    digit1and2.put(13, "externe Verlegung zur psychiatrischen Behandlung");
    digit1and2.put(
        14, "Behandlung aus sonstigen Gründen beendet, nachstationäre Behandlung vorgesehen");
    digit1and2.put(
        15, "Behandlung gegen ärztlichen Rat beendet, nachstationäre Behandlung vorgesehen");
    //    digit1and2.put(16,
    //        "externe Verlegung mit Rückverlegung oder Wechsel zwischen den Entgeltbereichen der
    // DRG-Fallpauschalen, nach der BPflV oder für besondere Einrichtungen nach § 17b Abs.1 Satz 15
    // KHG mit Rückverlegung");
    digit1and2.put(
        17,
        "interne Verlegung mit Wechsel zwischen den Entgeltbereichen der DRG-Fallpauschalen, "
            + "nach der BPflV oder für besondere Einrichtungen nach § 17b Abs.1 Satz 15 KHG");
    //    digit1and2.put(18, "Rückverlegung");
    //    digit1and2.put(19, "Entlassung vor Wiederaufnahme mit Neueinstufung");
    //    digit1and2.put(20, "Entlassung vor Wiederaufnahme mit Neueinstufung wegen Komplikation");
    //    digit1and2.put(21, "Entlassung oder Verlegung mit nachfolgender Wiederaufnahme");
    digit1and2.put(
        22,
        "Fallabschluss (interne Verlegung) bei Wechsel zwischen voll- und teilstationärer "
            + "Behandlung");
    digit1and2.put(
        23,
        "Beginn eines externen Aufenthalts mit Abwesenheit über Mitternacht (BPflV-Bereich, für "
            + "verlegende Fachabteilung)");
    digit1and2.put(
        24,
        "Beendigung eines externen Aufenthalts mit Abwesenheit über Mitternacht (BPflV-Bereich, "
            + "für Pseudo-Fachabteilung 0003)");
    digit1and2.put(
        25,
        "Entlassung zum Jahresende bei Aufnahme im Vorjahr (für Zwecke der Abrechnung, "
            + "§ 4 PEPPV)");
    digit1and2.put(
        26,
        "Beginn eines Zeitraumes ohne direkten Patientenkontakt (stationsäquivalente "
            + "Behandlung)");
    digit1and2.put(
        27,
        "Beendigung eines Zeitraumes ohne direkten Patientenkontakt (stationsäquivalente "
            + "Behandlung – für Pseudofachabteilung 0004)");

    digit3.put(1, "arbeitsfähig entlassen");
    digit3.put(2, "arbeitsunfähig entlassen");
    digit3.put(9, "keine Angabe");

    codesWithAllSubcodes.add(1);
    codesWithAllSubcodes.add(2);
    codesWithAllSubcodes.add(3);
    codesWithAllSubcodes.add(4);
    codesWithAllSubcodes.add(14);
    codesWithAllSubcodes.add(15);
    codesWithAllSubcodes.add(21);
  }

  /**
   * TODO: add javadoc.
   */
  public static void main(String[] args) throws IOException, JAXBException {
    Catalog catalog = createCatalogSkeleton();

    addCodesToCatalog(catalog);
    createHierarchy(catalog);

    writeToDisk(catalog, RESULT_FILENAME);
  }

  private static Catalog createCatalogSkeleton() {
    final Catalog catalog = new Catalog();
    final Definitions definitions = new Definitions();
    Definition deDefinition = new Definition();
    deDefinition.setDesignation("Entlassungs-/Verlegungsgrund ");
    deDefinition.setDefinition("Entlassungs-/Verlegungsgrund nach §21 KHEntgG");
    deDefinition.setLang("de");

    definitions.getDefinition().add(deDefinition);
    catalog.setDefinitions(definitions);
    return catalog;
  }

  private static void addCodesToCatalog(Catalog catalog) {
    Iterator it = digit1and2.entrySet().iterator();

    // First, create the categories (first two digits)
    while (it.hasNext()) {
      Entry pair = (Entry) it.next();
      Code code = createTopLevelCodeFromMapEntry(pair);
      catalog.getCode().add(code);
    }

    // Now create all the subcategories. Yes it is inefficient to iterate through the map twice,
    // but it is insignificant
    it = digit1and2.entrySet().iterator();
    while (it.hasNext()) {
      Entry<Integer, String> pair = (Entry) it.next();
      List<Code> codes = createSubCodesFromMapEntries(pair);
      catalog.getCode().addAll(codes);
    }
  }

  private static Code createTopLevelCodeFromMapEntry(Entry<Integer, String> pair) {
    String formattedCode = String.format("%02d", pair.getKey());

    final Code code = new Code();
    final Definitions definitions = new Definitions();
    final Definition definition = new Definition();

    code.setCode(formattedCode);
    code.setOrder(pair.getKey());

    definition.setDesignation(pair.getValue());
    definition.setDefinition(formattedCode + " - " + pair.getValue());
    definition.setLang("de");

    definitions.getDefinition().add(definition);
    code.setDefinitions(definitions);

    final Slots slots = new Slots();
    final Slot slotPath = new Slot();
    final Slot slotIcon = new Slot();
    final Slot slotHlevel = new Slot();

    slotPath.setKey(SLOTNAME_I2B2_PATH);
    slotIcon.setKey(SLOTNAME_I2B2_ICON);
    slotHlevel.setKey(SLOTNAME_I2B2_HLEVEL);

    slotPath.setValue(SLOT_I2B2_PATH_PREFIX + formattedCode + "\\");
    slotHlevel.setValue("3");
    slotIcon.setValue("FA");

    slots.getSlot().add(slotPath);
    slots.getSlot().add(slotIcon);
    slots.getSlot().add(slotHlevel);
    code.setSlots(slots);
    return code;
  }

  private static List<Code> createSubCodesFromMapEntries(Entry<Integer, String> pair) {
    List<Code> codes = new ArrayList<>();
    String formattedTopLevelCode = String.format("%02d", pair.getKey());

    Iterator it = digit3.entrySet().iterator();

    while (it.hasNext()) {
      Entry<Integer, String> entry = (Entry) it.next();

      // If I interpret the document correctly, the "9" as 3rd digit is valid for ALL entries
      // Only "1" and "2" are limited to those "main" codes in the set
      if (entry.getKey() != 9 && !codesWithAllSubcodes.contains(pair.getKey())) {
        continue;
      }

      String subcode = Integer.toString(entry.getKey());
      String formattedCode = formattedTopLevelCode + subcode;

      Code code = new Code();
      final Definitions definitions = new Definitions();
      final Definition definition = new Definition();

      code.setCode(formattedCode);
      code.setOrder(entry.getKey());
      code.setIsValid(true);

      String designationString = pair.getValue() + " - " + entry.getValue();
      definition.setDesignation(designationString);
      definition.setDefinition(formattedCode + " - " + designationString);
      definition.setLang("de");

      definitions.getDefinition().add(definition);
      code.setDefinitions(definitions);

      final Slots slots = new Slots();
      final Slot slotPath = new Slot();
      final Slot slotIcon = new Slot();
      final Slot slotHlevel = new Slot();

      slotPath.setKey(SLOTNAME_I2B2_PATH);
      slotIcon.setKey(SLOTNAME_I2B2_ICON);
      slotHlevel.setKey(SLOTNAME_I2B2_HLEVEL);

      slotPath.setValue(SLOT_I2B2_PATH_PREFIX + formattedTopLevelCode + "\\" + subcode + "\\");
      slotHlevel.setValue("4");
      slotIcon.setValue("LA");

      slots.getSlot().add(slotPath);
      slots.getSlot().add(slotIcon);
      slots.getSlot().add(slotHlevel);
      code.setSlots(slots);

      codes.add(code);
    }

    return codes;
  }

  private static void createHierarchy(Catalog catalog) {
    for (Code parentCode : catalog.getCode()) {
      // Take all 2-digit codes and search for codes starting with the same 2 digits
      if (parentCode.getCode() == null
          || parentCode.getCode().isEmpty()
          || parentCode.getCode().length() != 2) {
        continue;
      }

      for (Code childCode : catalog.getCode()) {
        if (childCode.getCode() == null
            || childCode.getCode().isEmpty()
            || childCode.getCode().length() != 3) {
          continue;
        }
        if (childCode.getCode().startsWith(parentCode.getCode())) {
          SubCode subCode = new SubCode();
          subCode.setCode(childCode.getCode());
          parentCode.getSubCode().add(subCode);
        }
      }
    }
  }

  private static void writeToDisk(Catalog catalog, String filename)
      throws IOException, JAXBException {

    Path pathToFile = Paths.get(RESULT_FILENAME);
    Files.createDirectories(pathToFile.getParent());

    Writer writer = new OutputStreamWriter(new FileOutputStream(filename), StandardCharsets.UTF_8);
    writer.write(
        JaxbUtil.marshall(
            new ObjectFactory().createCatalog(catalog),
            JAXBContext.newInstance(ObjectFactory.class)));

    writer.close();
  }
}
