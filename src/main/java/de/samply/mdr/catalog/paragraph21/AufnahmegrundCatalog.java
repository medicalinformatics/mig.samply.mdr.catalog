/*
 * Copyright (C) 2019 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.catalog.paragraph21;

import de.samply.config.util.JaxbUtil;
import de.samply.mdr.xsd.Catalog;
import de.samply.mdr.xsd.Code;
import de.samply.mdr.xsd.Code.SubCode;
import de.samply.mdr.xsd.Definition;
import de.samply.mdr.xsd.Definitions;
import de.samply.mdr.xsd.ObjectFactory;
import de.samply.mdr.xsd.Slot;
import de.samply.mdr.xsd.Slots;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

public class AufnahmegrundCatalog {

  public static final String RESULT_FILENAME = "target/generated-resources/xml/Aufnahmegrund.xml";
  private static final String SLOTNAME_I2B2_OPERATOR = "I2B2_OPERATOR";
  private static final String SLOTNAME_I2B2_PATH = "I2B2_PATH";
  private static final String SLOTNAME_I2B2_ICON = "I2B2_ICON";
  private static final String SLOTNAME_I2B2_HLEVEL = "I2B2_HLEVEL";

  private static final String SLOT_I2B2_PATH_PREFIX = "\\i2b2\\ADM\\AUFNGR\\";

  private static Map<Integer, String> digit1and2 = new HashMap<>();
  private static Map<Integer, String> digit3 = new HashMap<>();
  private static Map<Integer, String> digit4 = new HashMap<>();

  static {
    digit1and2.put(1, "Krankenhausbehandlung, vollstationär");
    digit1and2.put(
        2, "Krankenhausbehandlung vollstationär mit vorausgegangener vorstationärer Behandlung");
    digit1and2.put(3, "Krankenhausbehandlung, teilstationär");
    digit1and2.put(4, "Vorstationäre Behandlung ohne anschließende vollstationäre Behandlung");
    digit1and2.put(5, "Stationäre Entbindung");
    digit1and2.put(6, "Geburt");
    // According to the §21 Document, "07" is not used, this is only in "Anlage 2 zur
    // §301-Vereinbarung"
    // From my understanding, it should not be in here then.
    //    digit1and2.put(7, "Wiederaufnahme wegen Komplikationen (Fallpauschale) nach KFPV 2003");
    digit1and2.put(8, "Stationäre Aufnahme zur Organentnahme");
    // According to the document, "09" is "-frei-" as in "not assigned"
    digit1and2.put(10, "Stationsäquivalente Behandlung");

    digit3.put(0, "");
    digit3.put(2, " (bei tagesbezogenen Entgelten)");
    digit3.put(4, " (im Rahmen von Verträgen zur integrierten Versorgung)");

    digit4.put(1, "Normalfall");
    digit4.put(2, "Arbeitsunfall / Berufskrankheit (§ 11 Abs. 5 SGB V)");
    digit4.put(3, "Verkehrsunfall / Sportunfall / Sonstiger Unfall (z. B. § 116 SGB X)");
    digit4.put(4, "Hinweis auf Einwirkung von äußerer Gewalt");
    // According to the document, "05" is "-frei-" as in "not assigned"
    digit4.put(6, "Kriegsbeschädigten-Leiden / BVG-Leiden");
    digit4.put(7, "Notfall");
  }

  /**
   * TODO: add javadoc.
   */
  public static void main(String[] args) throws IOException, JAXBException {
    Catalog catalog = createCatalogSkeleton();

    addCodesToCatalog(catalog);
    createHierarchy(catalog);

    writeToDisk(catalog, RESULT_FILENAME);
  }

  private static Catalog createCatalogSkeleton() {
    Definition deDefinition = new Definition();
    deDefinition.setDesignation("Aufnahmegrund");
    deDefinition.setDefinition("Aufnahmegrund nach §21 KHEntgG");
    deDefinition.setLang("de");

    Catalog catalog = new Catalog();
    Definitions definitions = new Definitions();
    definitions.getDefinition().add(deDefinition);
    catalog.setDefinitions(definitions);
    return catalog;
  }

  private static void addCodesToCatalog(Catalog catalog) {
    Iterator it = digit1and2.entrySet().iterator();

    // First, create the categories (first two digits)
    while (it.hasNext()) {
      Map.Entry pair = (Map.Entry) it.next();
      Code code = createTopLevelCodeFromMapEntry(pair);
      catalog.getCode().add(code);
    }

    // Now create all the subcategories. Yes it is inefficient to iterate through the map twice,
    // but it is insignificant
    it = digit1and2.entrySet().iterator();
    while (it.hasNext()) {
      Map.Entry<Integer, String> pair = (Map.Entry) it.next();
      Iterator it2 = digit4.entrySet().iterator();
      while (it2.hasNext()) {
        Map.Entry pair2 = (Map.Entry) it2.next();
        List<Code> codes = createSubCodesFromMapEntries(pair, pair2);
        catalog.getCode().addAll(codes);
      }
    }
  }

  private static Code createTopLevelCodeFromMapEntry(Entry<Integer, String> pair) {
    String formattedCode = String.format("%02d", pair.getKey());

    final Code code = new Code();
    final Definitions definitions = new Definitions();
    final Definition definition = new Definition();

    code.setCode(formattedCode);
    code.setOrder(pair.getKey());

    definition.setDesignation(pair.getValue());
    definition.setDefinition(formattedCode + " - " + pair.getValue());
    definition.setLang("de");

    definitions.getDefinition().add(definition);
    code.setDefinitions(definitions);

    final Slots slots = new Slots();
    final Slot slotPath = new Slot();
    final Slot slotIcon = new Slot();
    final Slot slotHlevel = new Slot();

    slotPath.setKey(SLOTNAME_I2B2_PATH);
    slotIcon.setKey(SLOTNAME_I2B2_ICON);
    slotHlevel.setKey(SLOTNAME_I2B2_HLEVEL);

    slotPath.setValue(SLOT_I2B2_PATH_PREFIX + formattedCode + "\\");
    slotHlevel.setValue("3");
    slotIcon.setValue("FA");

    slots.getSlot().add(slotPath);
    slots.getSlot().add(slotIcon);
    slots.getSlot().add(slotHlevel);
    code.setSlots(slots);
    return code;
  }

  private static List<Code> createSubCodesFromMapEntries(
      Entry<Integer, String> pair, Entry<Integer, String> pair2) {
    List<Code> codes = new ArrayList<>();
    String formattedTopLevelCode = String.format("%02d", pair.getKey());
    Iterator it = digit3.entrySet().iterator();

    while (it.hasNext()) {
      Map.Entry<Integer, String> entry3 = (Map.Entry) it.next();
      String subcode = Integer.toString(entry3.getKey()) + Integer.toString(pair2.getKey());
      String formattedCode = formattedTopLevelCode + subcode;

      final Code code = new Code();
      final Definitions definitions = new Definitions();
      final Definition definition = new Definition();

      code.setCode(formattedCode);

      /*
       * When ordering the codes, group them by their 4th digit first, so the order is
       * 0402 > 0422 > 0442 > 0403 > 0423 > 0443...
       */
      Integer order = pair.getKey() * 100 + pair2.getKey() * 10 + entry3.getKey();
      code.setOrder(order);
      code.setIsValid(true);

      String designationString = pair.getValue() + " - " + pair2.getValue() + entry3.getValue();
      definition.setDesignation(designationString);
      definition.setDefinition(formattedCode + " - " + designationString);
      definition.setLang("de");

      definitions.getDefinition().add(definition);
      code.setDefinitions(definitions);

      final Slots slots = new Slots();
      final Slot slotPath = new Slot();
      final Slot slotIcon = new Slot();
      final Slot slotHlevel = new Slot();

      slotPath.setKey(SLOTNAME_I2B2_PATH);
      slotIcon.setKey(SLOTNAME_I2B2_ICON);
      slotHlevel.setKey(SLOTNAME_I2B2_HLEVEL);

      slotPath.setValue(SLOT_I2B2_PATH_PREFIX + formattedTopLevelCode + "\\" + subcode + "\\");
      slotHlevel.setValue("4");
      slotIcon.setValue("LA");

      slots.getSlot().add(slotPath);
      slots.getSlot().add(slotIcon);
      slots.getSlot().add(slotHlevel);
      code.setSlots(slots);

      codes.add(code);
    }

    return codes;
  }

  private static void createHierarchy(Catalog catalog) {
    for (Code parentCode : catalog.getCode()) {
      // Take all 2-digit codes and search for codes starting with the same 2 digits
      if (parentCode.getCode() == null
          || parentCode.getCode().isEmpty()
          || parentCode.getCode().length() != 2) {
        continue;
      }

      for (Code childCode : catalog.getCode()) {
        if (childCode.getCode() == null
            || childCode.getCode().isEmpty()
            || childCode.getCode().length() != 4) {
          continue;
        }
        if (childCode.getCode().startsWith(parentCode.getCode())) {
          SubCode subCode = new SubCode();
          subCode.setCode(childCode.getCode());
          parentCode.getSubCode().add(subCode);
        }
      }
    }
  }

  private static void writeToDisk(Catalog catalog, String filename)
      throws IOException, JAXBException {

    Path pathToFile = Paths.get(RESULT_FILENAME);
    Files.createDirectories(pathToFile.getParent());

    Writer writer = new OutputStreamWriter(new FileOutputStream(filename), StandardCharsets.UTF_8);
    writer.write(
        JaxbUtil.marshall(
            new ObjectFactory().createCatalog(catalog),
            JAXBContext.newInstance(ObjectFactory.class)));

    writer.close();
  }
}
