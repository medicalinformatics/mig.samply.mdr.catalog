/*
 * Copyright (C) 2019 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.catalog;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.json.JSONArray;
import org.json.XML;

/** Converts the JSON file from https://github.com/mledoze/countries to XML format. */
public class CountriesJsonToXml {

  private static final Path OUTPUT_PATH = Paths.get("target/generated-resources/xml");

  /**
   * TODO: add javadoc.
   */
  public static void main(String[] args) throws IOException {
    InputStream inputStream =
        CountriesJsonToXml.class.getClassLoader().getResourceAsStream("countries.json");
    if (inputStream == null) {
      inputStream =
          CountriesJsonToXml.class.getClassLoader().getResourceAsStream("countries-unescaped.json");
    }
    if (inputStream == null) {
      System.out.println("Source file not found.");
    }

    BufferedReader streamReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
    StringBuilder stringBuilder = new StringBuilder();

    String inputStr;
    while ((inputStr = streamReader.readLine()) != null) {
      stringBuilder.append(inputStr);
    }

    JSONArray json = new JSONArray(stringBuilder.toString());

    if (Files.notExists(OUTPUT_PATH)) {
      Files.createDirectories(OUTPUT_PATH);
    }

    Writer writer =
        new OutputStreamWriter(
            new FileOutputStream(OUTPUT_PATH + File.separator + "countries_json.xml"),
            StandardCharsets.UTF_8);
    writer.write(
        "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<countries>"
            + XML.toString(json, "country")
            + "</countries>");

    writer.close();
  }
}
