/*
 * Copyright (C) 2019 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.catalog;

import de.samply.mdr.catalog.i2b2.I2b2SlotValue;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Add I2B2-specific information to slots in icd10 catalog.
 *
 * <p>Requires converted icd10 catalog (run mvn xml:transform in this project) and i2b2 ontology
 * dump
 */
public class I2b2Icd10Catalog {

  private static final String I2B2_INPUT_FILENAME = "i2b2_dump_ontology_terminologies.csv";
  private static final String MDR_ICD10_CATALOG_FILENAME = "src/main/resources/icd10gm2018.xml";
  private static final String MDR_ICD10_CATALOG_WITH_SLOTS_FILENAME =
      "target/icd10gm2018_i2b2slots.xml";
  private static final String ICD10_PATH_START = "\\i2b2\\ADM\\DIA\\";
  private static final String NODENAME_CODE = "code";
  private static final String NODENAME_SUBCODE = "subCode";
  private static final String NODENAME_SLOTS = "slots";
  private static final String NODENAME_SLOT = "slot";
  private static final String NODENAME_SLOTKEY = "key";
  private static final String NODENAME_SLOTVALUE = "value";
  private static final String ATTRIBUTE_CODE = "code";
  private static final String SLOT_KEY_PATH = "I2B2_PATH";
  private static final String SLOT_KEY_OPERATOR = "I2B2_OPERATOR";
  private static final String SLOT_KEY_HLEVEL = "I2B2_HLEVEL";
  private static final String SLOT_KEY_ICON = "I2B2_ICON";

  /**
   * TODO: add javadoc.
   */
  public static void main(String[] args)
      throws IOException, ParserConfigurationException, SAXException, TransformerException {
    Map<String, I2b2SlotValue> codeMap = getI2b2ValuesFromCsv(I2B2_INPUT_FILENAME);
    Document icd10catalog = parseCatalog(MDR_ICD10_CATALOG_FILENAME);
    Document modifiedCatalog = modifyCatalog(icd10catalog, codeMap);
    saveCatalog(modifiedCatalog, MDR_ICD10_CATALOG_WITH_SLOTS_FILENAME);
  }

  private static Document modifyCatalog(Document icd10catalog, Map<String, I2b2SlotValue> codeMap) {
    Element root = icd10catalog.getDocumentElement();

    NodeList codeElements = root.getElementsByTagName(NODENAME_CODE);

    for (int i = 0; i < codeElements.getLength(); ++i) {
      Element codeElement = (Element) codeElements.item(i);
      I2b2SlotValue slotValue = codeMap.get(codeElement.getAttribute(ATTRIBUTE_CODE));
      if (slotValue != null) {
        // If there are subCode elements, insert the slots before them, since it is a sequence in
        // the xsd
        // (order of the element matters)
        NodeList subCodeElements = codeElement.getElementsByTagName(NODENAME_SUBCODE);
        if (subCodeElements != null && subCodeElements.getLength() > 0) {
          codeElement.insertBefore(
              createSlotsElement(icd10catalog, slotValue), subCodeElements.item(0));
        } else {
          codeElement.appendChild(createSlotsElement(icd10catalog, slotValue));
        }
      }
    }

    return icd10catalog;
  }

  private static Element createSlotsElement(Document document, I2b2SlotValue slotValue) {


    Element slotKeyElement = document.createElement(NODENAME_SLOTKEY);
    slotKeyElement.appendChild(document.createTextNode(SLOT_KEY_HLEVEL));
    Element slotValueElement = document.createElement(NODENAME_SLOTVALUE);
    slotValueElement.appendChild(document.createTextNode(slotValue.getHlevel()));
    Element slotElement = document.createElement(NODENAME_SLOT);
    slotElement.appendChild(slotKeyElement);
    slotElement.appendChild(slotValueElement);
    Element slotsElement = document.createElement(NODENAME_SLOTS);
    slotsElement.appendChild(slotElement);

    slotElement = document.createElement(NODENAME_SLOT);
    slotKeyElement = document.createElement(NODENAME_SLOTKEY);
    slotValueElement = document.createElement(NODENAME_SLOTVALUE);
    slotKeyElement.appendChild(document.createTextNode(SLOT_KEY_ICON));
    slotValueElement.appendChild(document.createTextNode(slotValue.getIcon().trim()));
    slotElement.appendChild(slotKeyElement);
    slotElement.appendChild(slotValueElement);
    slotsElement.appendChild(slotElement);

    slotElement = document.createElement(NODENAME_SLOT);
    slotKeyElement = document.createElement(NODENAME_SLOTKEY);
    slotValueElement = document.createElement(NODENAME_SLOTVALUE);
    slotKeyElement.appendChild(document.createTextNode(SLOT_KEY_PATH));
    slotValueElement.appendChild(document.createTextNode(slotValue.getPath()));
    slotElement.appendChild(slotKeyElement);
    slotElement.appendChild(slotValueElement);
    slotsElement.appendChild(slotElement);

    slotElement = document.createElement(NODENAME_SLOT);
    slotKeyElement = document.createElement(NODENAME_SLOTKEY);
    slotValueElement = document.createElement(NODENAME_SLOTVALUE);
    slotKeyElement.appendChild(document.createTextNode(SLOT_KEY_OPERATOR));
    slotValueElement.appendChild(document.createTextNode(slotValue.getOperator()));
    slotElement.appendChild(slotKeyElement);
    slotElement.appendChild(slotValueElement);
    slotsElement.appendChild(slotElement);

    return slotsElement;
  }

  private static void saveCatalog(Document icd10catalog, String filename)
      throws TransformerException {
    DOMSource source = new DOMSource(icd10catalog);

    TransformerFactory transformerFactory = TransformerFactory.newInstance();
    Transformer transformer = transformerFactory.newTransformer();
    transformer.setOutputProperty(OutputKeys.INDENT, "yes");
    transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
    StreamResult result = new StreamResult(filename);
    transformer.transform(source, result);
  }

  /** Read the prepared icd10 catalog file. */
  private static Document parseCatalog(String filename)
      throws ParserConfigurationException, IOException, SAXException {
    DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
    return documentBuilder.parse(filename);
  }

  /**
   * Read the (currently) required information from the i2b2 ontology dump.
   *
   * @param filename the filename of the i2b2 dump csv file
   * @return a map with icd10code->i2b2 info
   */
  private static Map<String, I2b2SlotValue> getI2b2ValuesFromCsv(String filename)
      throws IOException {
    InputStream fileStream = I2b2Icd10Catalog.class.getClassLoader().getResourceAsStream(filename);
    Map<String, I2b2SlotValue> codeMap = new HashMap<>();

    Reader in = new InputStreamReader(fileStream, StandardCharsets.UTF_8);
    CSVParser parser =
        new CSVParser(in, CSVFormat.EXCEL.withHeader(Headers.class).withDelimiter(';'));
    Iterable<CSVRecord> records = parser.getRecords();
    parser.close();

    for (CSVRecord record : records) {
      try {
        // Check if the path starts with \i2b2\ADM\DIA\ - those are the icd10 entries
        if (!record.get(Headers.PATH).startsWith(ICD10_PATH_START)) {
          continue;
        }
        String[] categoryParts = record.get(Headers.CATEGORY).split(":", 2);
        if (categoryParts == null || categoryParts.length < 2) {
          continue;
        }

        I2b2SlotValue slotValue = new I2b2SlotValue();

        slotValue.setHlevel(record.get(Headers.HLEVEL));
        slotValue.setIcon(record.get(Headers.ICON));
        slotValue.setPath(record.get(Headers.PATH));
        slotValue.setOperator(record.get(Headers.OPERATOR));

        codeMap.put(categoryParts[0], slotValue);
      } catch (IndexOutOfBoundsException e) {
        // Not a valid code entry, don't add anything to the map
      }
    }

    return codeMap;
  }

  /**
   * The file does not have headers. Define those needed, and leave the unused/unknown ones as U.
   */
  public enum Headers {
    HLEVEL,
    PATH,
    CATEGORY,
    U1,
    ICON,
    U2,
    CODE,
    U3,
    U4,
    U5,
    U6,
    U7,
    OPERATOR,
    U8,
    U9,
    U10,
    U11,
    U12,
    U13,
    U14,
    U15
  }
}
