# Copyright (C) 2019 The Samply Community
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation; either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program; if not, see <http://www.gnu.org/licenses>.
#
# Additional permission under GNU GPL version 3 section 7:
#
# If you modify this Program, or any covered work, by linking or combining it
# with Jersey (https://jersey.java.net) (or a modified version of that
# library), containing parts covered by the terms of the General Public
# License, version 2.0, the licensors of this Program grant you additional
# permission to convey the resulting work.

import http.client
import json
import os
import urllib.request
import xml.dom.minidom as minidom
import xml.etree.ElementTree as ET
from collections import defaultdict

DOMAIN = 'www.orphadata.org'
DATA = defaultdict(lambda: {
    'classified': False,
    'disorder_id': 0,
    'orpha_number': 0,
    'children': [],
    'name': {},
    'synonyms': {},
    'references': {}
})


def get_urls(product):
    connection = http.client.HTTPConnection(DOMAIN)
    connection.request(method='GET', url=('/cgi-bin/' + product))
    response = connection.getresponse().read()

    xml_urls = []
    json_array = json.loads(response)
    for json_object in json_array:
        xml_urls.append(json_object['anUrl'])
    return xml_urls


def run():
    CLASSIFICATION = os.path.join(os.path.split(__file__)[0], 'classification/')
    DISEASES = os.path.join(os.path.split(__file__)[0], 'diseases/')

    if not os.path.exists(CLASSIFICATION):
        print('Downloading classification ...')
        os.makedirs(CLASSIFICATION)
        [urllib.request.urlretrieve(url, CLASSIFICATION + url.split('/')[-1]) for url in
         get_urls('free_product3_class.json')]

    if not os.path.exists(DISEASES):
        print('Downloading diseases ...')
        os.makedirs(DISEASES)
        [urllib.request.urlretrieve(url, DISEASES + url.split('/')[-1]) for url in
         get_urls('free_product1_cross_xml.json') if 'cz' not in url]

    print('Processing classification ...')
    for (dirpath, dirnames, filenames) in os.walk(CLASSIFICATION):
        for filename in filenames:
            print('Processing ' + filename + ' ...')
            with open(dirpath + filename, 'rb') as file:
                parse_classification(file.read())
        break

    print('Processing diseases ...')
    for (dirpath, dirnames, filenames) in os.walk(DISEASES):
        for filename in filenames:
            print('Processing ' + filename + ' ...')
            with open(dirpath + filename, 'rb') as file:
                language = filename.split('_')[0]  # split url to find language prefix of file
                parse_diseases(file.read(), language)
        break

    print('Dumping to JSON file ...')
    with open('orphanet.json', 'w') as file:
        file.write(json.dumps(DATA, indent=2))

    print('Writing to XML file ...')
    write_to_xml()


def parse_classification(xml):
    root = ET.fromstring(xml).find('DisorderList').find('Disorder')
    disorder_id = root.attrib['id']
    nodes = root.find('ClassificationNodeList').find('ClassificationNode').find('ClassificationNodeChildList')

    element = DATA[disorder_id]
    element['disorder_id'] = disorder_id
    element['orpha_number'] = root.find('OrphaNumber').text
    element['children'] = [child.find('Disorder').find('OrphaNumber').text for child in
                           nodes.findall('ClassificationNode')]
    element['classified'] = True
    DATA[disorder_id] = element

    for node in nodes.iter('ClassificationNode'):
        disorder = node.find('Disorder')
        disorder_id = disorder.attrib['id']

        children = set([child.find('Disorder').find('OrphaNumber').text for child in
                        node.find('ClassificationNodeChildList').findall('ClassificationNode')])
        element = DATA[disorder_id]
        # check if node has different children in different roots
        # if you want to try this you need to change `list(children | set(element['children']))` to `list(children)`
        # if len(set(element['children'])) != 0 and children != set(element['children']):
        #     print(element['orpha_number'], ' ', children, ' ', element['children'])
        element['disorder_id'] = disorder_id
        element['orpha_number'] = disorder.find('OrphaNumber').text
        element['children'] = list(children | set(element['children']))
        element['classified'] = True
        DATA[disorder_id] = element


def parse_diseases(xml, language):
    root = ET.fromstring(xml).find('DisorderList')
    disorders = root.findall('Disorder')
    for disorder in disorders:
        disorder_id = disorder.attrib['id']

        synonyms = []
        synonymlist = disorder.find('SynonymList')
        if synonymlist:
            # we need to check this because in cz xml there is one disorder missing SynonymList (22476)
            for synonym in synonymlist.findall('Synonym'):
                synonyms.append(synonym.text)

        references = []
        for reference in disorder.find('ExternalReferenceList').findall('ExternalReference'):
            source = reference.find('Source').text
            id = reference.find('Reference').text
            references.append((source, id))

        element = DATA[disorder_id]
        element['disorder_id'] = disorder_id
        element['orpha_number'] = disorder.find('OrphaNumber').text
        element['name'][language] = disorder.find('Name').text
        if synonyms:
            element['synonyms'][language] = synonyms
        if references:
            for (source, id) in references:
                element['references'][source] = id
        DATA[disorder_id] = element


def write_to_xml():
    unclassified = []

    catalog = ET.Element('catalog', attrib={'xmlns': 'http://schema.samply.de/mdr/common'})
    definitions = ET.SubElement(catalog, 'definitions')
    definition = ET.SubElement(definitions, 'definition', attrib={'lang': 'en'})
    designation = ET.SubElement(definition, 'designation')
    designation.text = 'Orphanet Data'
    definition = ET.SubElement(definition, 'definition')
    definition.text = 'Orphanet Data'

    for disorder_id, data in DATA.items():
        oid = data['orpha_number']
        code = ET.SubElement(catalog, 'code', attrib={'code': oid, 'isValid': 'true'})
        definitions = ET.SubElement(code, 'definitions')
        for language, name in data['name'].items():
            definition = ET.SubElement(definitions, 'definition', attrib={'lang': language})
            designation = ET.SubElement(definition, 'designation')
            designation.text = name
            definition = ET.SubElement(definition, 'definition')
            definition.text = '{0}, {1}'.format(oid, name)

        slots = ET.SubElement(code, 'slots')
        slot = ET.SubElement(slots, 'slot')
        key = ET.SubElement(slot, 'key')
        key.text = 'disorder_id'
        value = ET.SubElement(slot, 'value')
        value.text = disorder_id

        if data['references']:
            slot = ET.SubElement(slots, 'slot')
            key = ET.SubElement(slot, 'key')
            key.text = 'references'
            value = ET.SubElement(slot, 'value')
            value.text = str(data['references'])

        if data['synonyms']:
            slot = ET.SubElement(slots, 'slot')
            key = ET.SubElement(slot, 'key')
            key.text = 'synonyms'
            value = ET.SubElement(slot, 'value')
            value.text = str(data['synonyms'])

        for child in data['children']:
            ET.SubElement(code, 'subCode', attrib={'code': child})

        if not data['classified']:
            unclassified.append(oid)

    # code is 0 because the unclassified element is not part of the orphanet classification and 0 is unused
    code = ET.SubElement(catalog, 'code', attrib={'code': '0', 'isValid': 'false'})
    definitions = ET.SubElement(code, 'definitions')
    definition = ET.SubElement(definitions, 'definition', attrib={'lang': 'en'})
    designation = ET.SubElement(definition, 'designation')
    designation.text = 'Unclassified'
    definition = ET.SubElement(definition, 'definition')
    definition.text = 'Unclassified'
    for child in unclassified:
        ET.SubElement(code, 'subCode', attrib={'code': child})

    with open('orphanet.xml', 'w') as file:
        file.write(minidom.parseString(ET.tostring(catalog, 'utf-8')).toprettyxml(indent='  '))


run()
