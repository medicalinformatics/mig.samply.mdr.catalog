# Samply MDR Catalog

This project contains several catalogs for the MDR:

- ICD-10-GM 2014
- ICD-10-GM 2018
- ICD-10-GM 2020
- ISO 2 letter codes for countries
- American and Canadian two letter codes for states and provinces
- The DKTK entity catalog

## Build

Use maven to build the XML catalogs:

```
mvn xml:transform
```
